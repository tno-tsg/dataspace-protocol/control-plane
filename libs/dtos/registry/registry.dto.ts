export interface CredentialAddressDto {
  didId: string;
  address: string;
}
